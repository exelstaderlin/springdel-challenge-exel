package com.example.springdelchallengeexel

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.wifi.WifiInfo
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Environment
import android.os.StatFs
import android.telephony.TelephonyManager
import android.text.format.Formatter
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.springdelchallengeexel.model.NetworkData
import java.io.File
import java.io.InputStream
import java.net.NetworkInterface
import java.util.*
import kotlin.math.pow
import kotlin.math.roundToInt


const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
const val DEFAULT_ZOOM = 15


object StorageUtil {


    fun getTotalStorage(context: Context): Long {
        val totalSize: Long =
            File(context.applicationContext.filesDir.absoluteFile.toString()).totalSpace
        return totalSize / (1024 * 1024) //MB
    }

    fun getFreeStorage(context: Context): Long {
        val availableSize: Long =
            File(context.applicationContext.filesDir.absoluteFile.toString()).freeSpace
        return availableSize / (1024 * 1024)
    }

    fun getTotalMemorySize(): Long {
        var totalSize = 0L
        try {
            val info = Runtime.getRuntime()
            totalSize = info.totalMemory()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return totalSize
    }

    fun getFreeMemorySize(): Long {
        var freeSize = 0L
        try {
            val info = Runtime.getRuntime()
            freeSize = info.freeMemory()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return freeSize
    }

    private fun externalMemoryAvailable(): Boolean {
        return Environment.getExternalStorageState() ==
                Environment.MEDIA_MOUNTED
    }

    fun getAvailableExternalMemorySize(): String? {
        return if (externalMemoryAvailable()) {
            val path = Environment.getExternalStorageDirectory()
            val stat = StatFs(path.path)
            val blockSize = stat.blockSizeLong
            val availableBlocks = stat.availableBlocksLong
            formatSizeToGiga(availableBlocks * blockSize)
        } else {
            "ERROR"
        }
    }

    fun getTotalExternalMemorySize(): String? {
        return if (externalMemoryAvailable()) {
            val path = Environment.getExternalStorageDirectory()
            val stat = StatFs(path.path)
            val blockSize = stat.blockSizeLong
            val totalBlocks = stat.blockCountLong
            formatSizeToGiga(totalBlocks * blockSize)
        } else {
            "ERROR"
        }
    }


    fun formatSizeToGiga(size: Long): String? {
        var size: Double = size.toDouble()
        if (size >= 1024) {
            size /= 1024 //KB
            if (size >= 1024) {
                size /= 1024 //MB
                if (size >= 1024) {
                    size /= 1024 //GB
                }
            }
        }
        val limitSize = limitDigit(size)
        val resultBuffer =
            StringBuilder(limitSize.toString())
//        var commaOffset = resultBuffer.length - 3
//        while (commaOffset > 0) {
//            resultBuffer.insert(commaOffset, ',')
//            commaOffset -= 3
//        }
        return resultBuffer.toString()
    }

    private fun limitDigit(value: Double): Double {
        return (value * 10.0.pow(2)).roundToInt() / 10.0.pow(2)
    }

    fun getCpuInfoDetail(): String {
        var output = ""
        try {
            val data =
                arrayOf("/system/bin/cat", "/proc/cpuinfo")
            val processBuilder = ProcessBuilder(*data)
            val process = processBuilder.start()
            val inputStream: InputStream = process.inputStream
            val byteArr = ByteArray(1024)
            while (inputStream.read(byteArr) != -1) {
                output += String(byteArr)
            }
            inputStream.close()
            Log.d("CPU_INFO", output)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return output
    }

    fun getConnectivityInformationData(context: Context): NetworkData {
        val networkInfoData = NetworkData()
        val wifiManager =
            context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager //WIFI CONNECTION
        val wifiInfo = wifiManager.connectionInfo
        if (wifiInfo != null) {
            val state = WifiInfo.getDetailedStateOf(wifiInfo.supplicantState)
            if (state == NetworkInfo.DetailedState.CONNECTED || state == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
                val ipAddress = Formatter.formatIpAddress(wifiInfo.ipAddress)
                networkInfoData.wifiName = wifiInfo.ssid
                networkInfoData.ipAddress = wifiInfo.ipAddress.toString()
                networkInfoData.macAddress = getMacAddr() ?: ""
                networkInfoData.linkSpeed = "${wifiInfo.linkSpeed} Mbps"
            }
        }

        val tManager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager //MOBILE CONNECTION
        val operatorName = tManager.networkOperatorName
        val networkType: String = when(tManager.networkType) {
            TelephonyManager.NETWORK_TYPE_EDGE -> "EDGE"
            TelephonyManager.NETWORK_TYPE_CDMA -> "CDMA"
            TelephonyManager.NETWORK_TYPE_IDEN -> "2G"
            TelephonyManager.NETWORK_TYPE_HSPAP -> "3G"
            TelephonyManager.NETWORK_TYPE_LTE -> "4G"
            else -> "NotFound"
        }

        networkInfoData.operatorName = operatorName
        networkInfoData.networkType = networkType
        return networkInfoData
    }

    fun getLocationPermission(activity: Activity, granted: () -> Unit) {
        if (ContextCompat.checkSelfPermission(
                activity.applicationContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            granted()
        } else {
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }

    private fun getMacAddr(): String? {
        try {
            val all: List<NetworkInterface> =
                Collections.list(NetworkInterface.getNetworkInterfaces())
            for (nif in all) {
                if (nif.name != "wlan0") continue
                val macBytes: ByteArray = nif.hardwareAddress ?: return ""
                val res1 = java.lang.StringBuilder()
                for (b in macBytes) {
                    var hex = Integer.toHexString(b.toInt() and 0xFF)
                    if (hex.length == 1) hex = "0$hex"
                    res1.append("$hex:")
                }
                if (res1.isNotEmpty()) {
                    res1.deleteCharAt(res1.length - 1)
                }
                return res1.toString()
            }
        } catch (ex: java.lang.Exception) {
        }
        return ""
    }


}