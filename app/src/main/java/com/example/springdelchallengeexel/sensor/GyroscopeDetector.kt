package com.example.springdelchallengeexel.sensor

import android.hardware.Sensor.TYPE_GYROSCOPE
import android.hardware.SensorEvent

/**
 * Created by Exel staderlin on 3/19/2019.
 */
class GyroscopeDetector(
    private var gyroListener: GyroscopeListener
) : SensorDetector(TYPE_GYROSCOPE) {

    interface GyroscopeListener {
        fun onMove(value : Float)
    }

    override fun onSensorEvent(sensorEvent: SensorEvent) {
        gyroListener.onMove(sensorEvent.values[2])
    }

}