package com.example.springdelchallengeexel.sensor

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener

/**
 * Created by Exel staderlin on 3/19/2019.
 */

abstract class SensorDetector(private vararg val sensorTypes: Int) : SensorEventListener {

    override fun onAccuracyChanged(sensor: Sensor, i: Int) {}

    override fun onSensorChanged(sensorEvent: SensorEvent) {
        if (isSensorEventBelongsToPluggedTypes(sensorEvent)) {
            onSensorEvent(sensorEvent)
        }
    }

    fun getSensorTypes(): IntArray {
        return sensorTypes
    }

    open fun onSensorEvent(sensorEvent: SensorEvent) {
    }

    private fun isSensorEventBelongsToPluggedTypes(sensorEvent: SensorEvent): Boolean {
        for (sensorType in sensorTypes) {
            if (sensorEvent.sensor.type == sensorType) {
                return true
            }
        }
        return false
    }
}