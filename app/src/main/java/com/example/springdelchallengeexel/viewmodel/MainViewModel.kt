package com.example.springdelchallengeexel.viewmodel

import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.springdelchallengeexel.model.AppInfo
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.Collator
import java.util.*
import kotlin.collections.ArrayList

class MainViewModel : ViewModel() {

    val appLiveData = MutableLiveData<List<AppInfo>>()

    fun getAllInstalledApps(context: Context, includeSystem: Boolean) =
        launchDataLoad(context, includeSystem) { appList ->
            appLiveData.postValue(appList)
        }

    private fun launchDataLoad(
        context: Context,
        includeSystem: Boolean,
        block: suspend (appList: List<AppInfo>) -> Unit
    ) {
        GlobalScope.launch {
            try {
                val appList: MutableList<AppInfo> = ArrayList()
                val pm: PackageManager = context.packageManager
                val infos = PackageManager.GET_META_DATA.let { pm.getInstalledApplications(it) }
                for (info in infos) {
                    if (!includeSystem && info.flags and ApplicationInfo.FLAG_SYSTEM == 1) {
                        continue
                    }
                    val app = AppInfo()
                    app.label = info.loadLabel(pm) as String
                    app.packageName = info.packageName
                    app.icon = info!!.loadIcon(pm)
                    appList.add(app)
                }
                Collections.sort(appList, DisplayNameComparator())
                block(appList)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    inner class DisplayNameComparator : Comparator<AppInfo?> {
        override fun compare(aa: AppInfo?, ab: AppInfo?): Int {
            val sa: String? = aa?.label
            val sb: String? = ab?.label
            return Collator.getInstance().compare(sa, sb)
        }
    }


}