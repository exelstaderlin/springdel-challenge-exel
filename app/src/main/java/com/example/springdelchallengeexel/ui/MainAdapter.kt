package com.example.springdelchallengeexel.ui

import android.annotation.SuppressLint
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.springdelchallengeexel.R
import com.example.springdelchallengeexel.model.AppInfo


class MainAdapter : ListAdapter<AppInfo, RecyclerView.ViewHolder>(DATA_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder( //Item layout
            layoutInflater.inflate(
                R.layout.card_app_list,
                parent,
                false
            )
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as ViewHolder
        val listApps = getItem(position)
        val packageManager = holder.itemView.context.packageManager
        holder.title.text = listApps.label
        try {
            val pi: PackageInfo = packageManager.getPackageInfo(listApps.packageName ?:"", 0)
            if (!TextUtils.isEmpty(pi.versionName)) {
                val versionInfo = String.format("%s", pi.versionName)
                holder.appVersion.text = versionInfo
            }
        } catch (e: PackageManager.NameNotFoundException) {
            holder.appVersion.text = e.toString()
        }
        if (!TextUtils.isEmpty(listApps.packageName)) {
            holder.subTitle.text = listApps.packageName
        }
        val background = listApps.icon
        holder.icon.background = background
        holder.itemView.setOnClickListener {
            val packageManager = holder.itemView.context.packageManager
            val app: AppInfo = listApps
            val intent = packageManager.getLaunchIntentForPackage(app.packageName.toString())
            if (intent != null) {
                holder.itemView.context.startActivity(intent)
            } else {
                Toast.makeText(
                    holder.itemView.context,
                    "No launch intent for " + app.packageName, Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var icon: ImageView = itemView.findViewById(R.id.icon)
        var title: TextView = itemView.findViewById(R.id.title)
        var appVersion: TextView = itemView.findViewById(R.id.appVersion)
        var subTitle: TextView = itemView.findViewById(R.id.subTitle)
    }

    companion object {
        //DiffUtil uses Eugene W. Myers's difference algorithm to calculate the minimal number of updates to convert one list into another.
        private val DATA_COMPARATOR = object : DiffUtil.ItemCallback<AppInfo>() {
            override fun areItemsTheSame(oldItem: AppInfo, newItem: AppInfo): Boolean =
                oldItem.label == newItem.label

            override fun areContentsTheSame(oldItem: AppInfo, newItem: AppInfo): Boolean =
                oldItem == newItem
        }
    }

}
