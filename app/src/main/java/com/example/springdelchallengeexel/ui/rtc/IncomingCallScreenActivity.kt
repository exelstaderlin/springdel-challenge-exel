package com.example.springdelchallengeexel.ui.rtc

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import com.example.springdelchallengeexel.R
import com.example.springdelchallengeexel.service.AudioPlayer
import com.example.springdelchallengeexel.service.SinchService
import com.sinch.android.rtc.PushPair
import com.sinch.android.rtc.calling.Call
import com.sinch.android.rtc.video.VideoCallListener
import kotlinx.android.synthetic.main.activity_incoming.*

class IncomingCallScreenActivity : RtcActivity() {
    private var mCallId: String? = null
    private var mAudioPlayer: AudioPlayer? = null
    private var mAcceptVideo = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_incoming)
        answerButton.setOnClickListener(mClickListener)
        declineButton.setOnClickListener(mClickListener)
        mAudioPlayer = AudioPlayer(this)
        mAudioPlayer!!.playRingtone()
        mCallId = intent.getStringExtra(SinchService.CALL_ID)
    }

    override fun onServiceConnected() {
        val call = sinchServiceInterface!!.getCall(mCallId)
        if (call != null) {
            call.addCallListener(SinchCallListener())
            val remoteUser = findViewById<TextView>(R.id.remoteUser)
            remoteUser.text = call.remoteUserId
        } else {
            Log.e(
                TAG,
                "Started with invalid callId, aborting"
            )
            finish()
        }
    }

    private fun answerClicked() {
        mAudioPlayer!!.stopRingtone()
        val call = sinchServiceInterface!!.getCall(mCallId)
        if (call != null) {
            call.answer()
            val intent = Intent(this, CallScreenActivity::class.java)
            intent.putExtra(SinchService.CALL_ID, mCallId)
            startActivity(intent)
        } else {
            finish()
        }
    }

    private fun declineClicked() {
        mAudioPlayer!!.stopRingtone()
        val call = sinchServiceInterface!!.getCall(mCallId)
        call?.hangup()
        finish()
    }

    private inner class SinchCallListener : VideoCallListener {
        override fun onCallEnded(call: Call) {
            val cause = call.details.endCause
            Log.d(
                TAG,
                "Call ended, cause: $cause"
            )
            mAudioPlayer!!.stopRingtone()
            finish()
        }

        override fun onCallEstablished(call: Call) {
            Log.d(TAG, "Call established")
        }

        override fun onCallProgressing(call: Call) {
            Log.d(TAG, "Call progressing")
        }

        override fun onShouldSendPushNotification(
            call: Call,
            pushPairs: List<PushPair>
        ) {
            // Send a push through your push provider here, e.g. GCM
        }

        override fun onVideoTrackAdded(call: Call) {
            // Display some kind of icon showing it's a video call
            // and pass it to the CallScreenActivity via Intent and mAcceptVideo
            mAcceptVideo = true
        }

        override fun onVideoTrackPaused(call: Call) {
            // Display some kind of icon showing it's a video call
        }

        override fun onVideoTrackResumed(call: Call) {
            // Display some kind of icon showing it's a video call
        }
    }

    private val mClickListener =
        View.OnClickListener { v ->
            when (v.id) {
                R.id.answerButton -> answerClicked()
                R.id.declineButton -> declineClicked()
            }
        }

    companion object {
        val TAG = IncomingCallScreenActivity::class.java.simpleName
    }
}