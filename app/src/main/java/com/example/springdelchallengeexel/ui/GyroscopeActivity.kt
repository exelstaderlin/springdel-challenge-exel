package com.example.springdelchallengeexel.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.springdelchallengeexel.R
import com.example.springdelchallengeexel.sensor.GyroscopeDetector
import com.example.springdelchallengeexel.sensor.SensorBuilder
import kotlinx.android.synthetic.main.activity_cpu_detail.*
import kotlinx.android.synthetic.main.activity_gyroscope.*


class GyroscopeActivity : AppCompatActivity(), GyroscopeDetector.GyroscopeListener {

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gyroscope)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        initSensor()
    }

    override fun onMove(value: Float) {
        if(value > 0.5f) { // anticlockwise
            direction.text = "Left"
        } else if(value < -0.5f) { // clockwise.
            direction.text = "Right"
        }
        value_sensor.text = value.toString()
    }

    private fun initSensor() {
        val sensorBuilder = SensorBuilder(this)
        sensorBuilder.setGyroListener(listener = this)
    }

}