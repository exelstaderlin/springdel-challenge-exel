package com.example.springdelchallengeexel.ui

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.springdelchallengeexel.DEFAULT_ZOOM
import com.example.springdelchallengeexel.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
import com.example.springdelchallengeexel.R
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_location_info.*
import java.util.*


class LocationActivity : AppCompatActivity(), OnMapReadyCallback {

    private var mFusedLocationProviderClient: FusedLocationProviderClient? = null
    private var mLocationPermissionGranted: Boolean = false
    private var mLastKnownLocation: Location? = null

    override fun onMapReady(map: GoogleMap?) {
        updateLocationUI(map)
        getDeviceLocation(map)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setContentView(R.layout.activity_location_info)
        getLocationPermission()
    }

    private fun initMaps() {
        mLocationPermissionGranted = true
        val mapFragment = map as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    @SuppressLint("SetTextI18n")
    private fun initView(vararg param: String) {
        address_tv.text = "Address : ${param[0]}"
        city_tv.text = "City : ${param[1]}"
        country_tv.text = "Country : ${param[2]}"
        latitude_tv.text = "Latitude : ${param[3]}"
        longitude_tv.text = "Longitude : ${param[4]}"
    }

    private fun getLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            initMaps()
            mFusedLocationProviderClient =
                LocationServices.getFusedLocationProviderClient(this)

        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }

    private fun updateLocationUI(mMap: GoogleMap?) {
        if (mMap == null) {
            return
        }
        try {
            if (mLocationPermissionGranted) {
                mMap!!.isMyLocationEnabled = true
                mMap!!.uiSettings.isMyLocationButtonEnabled = true
            } else {
                mMap!!.isMyLocationEnabled = false
                mMap!!.uiSettings.isMyLocationButtonEnabled = false
                mLastKnownLocation = null
                getLocationPermission()
            }
            mMap!!.setOnMyLocationButtonClickListener {

                getDeviceLocation(mMap)
                false
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message)
        }
    }

    private fun getDeviceLocation(mMap: GoogleMap?) {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            val geocoder = Geocoder(this, Locale.getDefault())
            if (mLocationPermissionGranted) {
                val locationResult = mFusedLocationProviderClient!!.lastLocation
                locationResult.addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Set the map's camera position to the current location of the device.
                        mLastKnownLocation = task.result
                        val mLat: Double = mLastKnownLocation?.latitude ?: 0.0
                        val mLng: Double = mLastKnownLocation?.longitude ?: 0.0
                        val listAddress = geocoder.getFromLocation(mLat, mLng, 1);
                        val address = listAddress[0].getAddressLine(0)
                        val city = listAddress[0].locality
                        val country = listAddress[0].countryName
                        mMap!!.moveCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                LatLng(
                                    mLat,
                                    mLng
                                ), DEFAULT_ZOOM.toFloat()
                            )
                        )
                        initView(address, city, country, mLat.toString(), mLng.toString())
                    } else {
                        Log.d("TAG", "Current location is null. Using defaults.")
                        Log.e("TAG", "Exception: %s", task.exception)
                        val mDefaultLocation = LatLng(0.0, 0.0)
                        mMap!!.moveCamera(
                            CameraUpdateFactory
                                .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM.toFloat())
                        )
                        mMap.uiSettings.isMyLocationButtonEnabled = false
                    }
                }
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message)
        }

    }

}