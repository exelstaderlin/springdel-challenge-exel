package com.example.springdelchallengeexel.ui

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.springdelchallengeexel.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
import com.example.springdelchallengeexel.R
import com.example.springdelchallengeexel.StorageUtil
import com.example.springdelchallengeexel.StorageUtil.getConnectivityInformationData
import com.example.springdelchallengeexel.model.NetworkData
import kotlinx.android.synthetic.main.activity_network_detail.*


class NetworkDetailActivity : AppCompatActivity() {

    private var networkData: NetworkData? = null

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    networkData = getConnectivityInformationData(this)
                }
            }

        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_network_detail)
        initObject()
        initView()
    }

    private fun initObject() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        networkData = NetworkData()
        StorageUtil.getLocationPermission(this) {
            networkData = getConnectivityInformationData(this)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initView() {
        wifiName.text = "Wifi : ${networkData?.wifiName}"
        ipAddress.text = "IP address : ${networkData?.ipAddress}"
        macAddress.text = "MAC address : ${networkData?.macAddress}"
        linkSpeed.text = "Link speed : ${networkData?.linkSpeed}"
        operatorName.text = "Mobile network : ${networkData?.operatorName}"
        networkType.text = "Network type : ${networkData?.networkType}"
    }
}