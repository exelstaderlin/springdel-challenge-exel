package com.example.springdelchallengeexel.ui.rtc

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.example.springdelchallengeexel.R
import com.example.springdelchallengeexel.service.SinchService.StartFailedListener
import com.sinch.android.rtc.SinchError
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : RtcActivity(), StartFailedListener {

    private var mProgressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loginButton.isEnabled = false
        loginButton.setOnClickListener { loginClicked() }
    }

    override fun onServiceConnected() {
        loginButton.isEnabled = true
        sinchServiceInterface!!.setStartListener(this)
    }

    override fun onPause() {
        if (mProgressDialog != null) {
            mProgressDialog!!.dismiss()
        }
        super.onPause()
    }

    override fun onStartFailed(error: SinchError?) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show()
        if (mProgressDialog != null) {
            mProgressDialog!!.dismiss()
        }
    }

    override fun onStarted() {
        openPlaceCallActivity()
    }

    private fun loginClicked() {
        val userName = loginName.text.toString()
        if (userName.isEmpty()) {
            Toast.makeText(this, "Please enter a name", Toast.LENGTH_LONG).show()
            return
        }
        if (userName != sinchServiceInterface!!.getUserName()) {
            sinchServiceInterface!!.stopClient()
        }
        if (!sinchServiceInterface!!.isStarted) {
            sinchServiceInterface!!.startClient(userName)
            showSpinner()
        } else {
            openPlaceCallActivity()
        }
    }

    private fun openPlaceCallActivity() {
        val mainActivity = Intent(this, PlaceCallActivity::class.java)
        startActivity(mainActivity)
    }

    private fun showSpinner() {
        mProgressDialog = ProgressDialog(this)
        mProgressDialog!!.setTitle("Logging in")
        mProgressDialog!!.setMessage("Please wait...")
        mProgressDialog!!.show()
    }
}