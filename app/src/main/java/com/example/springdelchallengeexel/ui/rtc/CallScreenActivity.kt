package com.example.springdelchallengeexel.ui.rtc

import android.media.AudioManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.example.springdelchallengeexel.R
import com.example.springdelchallengeexel.service.AudioPlayer
import com.example.springdelchallengeexel.service.SinchService
import com.sinch.android.rtc.PushPair
import com.sinch.android.rtc.calling.Call
import com.sinch.android.rtc.calling.CallState
import com.sinch.android.rtc.video.VideoCallListener
import kotlinx.android.synthetic.main.activity_callscreen.*
import java.util.*

class CallScreenActivity : RtcActivity() {
    private var mAudioPlayer: AudioPlayer? = null
    private var mTimer: Timer? = null
    private var mDurationTask: UpdateCallDurationTask? = null
    private var mCallId: String? = null
    private var mAddedListener = false
    private var mLocalVideoViewAdded = false
    private var mRemoteVideoViewAdded = false
    private var mToggleVideoViewPositions = false

    private inner class UpdateCallDurationTask : TimerTask() {
        override fun run() {
            runOnUiThread { updateCallDuration() }
        }
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        super.onSaveInstanceState(savedInstanceState)
        savedInstanceState.putBoolean(ADDED_LISTENER, mAddedListener)
        savedInstanceState.putBoolean(
            VIEWS_TOGGLED,
            mToggleVideoViewPositions
        )
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        mAddedListener = savedInstanceState.getBoolean(ADDED_LISTENER)
        mToggleVideoViewPositions =
            savedInstanceState.getBoolean(VIEWS_TOGGLED)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_callscreen)
        mAudioPlayer = AudioPlayer(this)
        val endCallButton =
            findViewById<Button>(R.id.hangupButton)
        endCallButton.setOnClickListener { endCall() }
        mCallId = intent.getStringExtra(SinchService.CALL_ID)
    }

    public override fun onServiceConnected() {
        val call = sinchServiceInterface!!.getCall(mCallId)
        if (!mAddedListener) {
            call.addCallListener(SinchCallListener())
            mAddedListener = true
        }
        updateUI()
    }

    private fun updateUI() {
        if (sinchServiceInterface == null) {
            return  // early
        }
        val call = sinchServiceInterface!!.getCall(mCallId)
        if (call != null) {
            remoteUser!!.text = call.remoteUserId
            callState!!.text = call.state.toString()
            if (call.details.isVideoOffered) {
                if (call.state == CallState.ESTABLISHED) {
                    setVideoViewsVisibility(true, true)
                } else {
                    setVideoViewsVisibility(true, false)
                }
            }
        } else {
            setVideoViewsVisibility(false, false)
        }
    }

    public override fun onStop() {
        super.onStop()
        mDurationTask!!.cancel()
        mTimer!!.cancel()
        removeVideoViews()
    }

    public override fun onStart() {
        super.onStart()
        mTimer = Timer()
        mDurationTask = UpdateCallDurationTask()
        mTimer!!.schedule(mDurationTask, 0, 500)
        updateUI()
    }

    override fun onBackPressed() {
        // User should exit activity by ending call, not by going back.
    }

    private fun endCall() {
        mAudioPlayer!!.stopProgressTone()
        val call = sinchServiceInterface!!.getCall(mCallId)
        call?.hangup()
        finish()
    }

    private fun formatTimespan(totalSeconds: Int): String {
        val minutes = totalSeconds / 60.toLong()
        val seconds = totalSeconds % 60.toLong()
        return String.format(Locale.US, "%02d:%02d", minutes, seconds)
    }

    private fun updateCallDuration() {
        val call = sinchServiceInterface!!.getCall(mCallId)
        if (call != null) {
            callDuration!!.text = formatTimespan(call.details.duration)
        }
    }

    private fun getVideoView(localView: Boolean): ViewGroup {
        var localView = localView
        if (mToggleVideoViewPositions) {
            localView = !localView
        }
        return if (localView) findViewById(R.id.localVideo) else findViewById(
            R.id.remoteVideo
        )
    }

    private fun addLocalView() {
        if (mLocalVideoViewAdded || sinchServiceInterface == null) {
            return  //early
        }
        val vc = sinchServiceInterface!!.videoController
        if (vc != null) {
            runOnUiThread {
                val localView = getVideoView(true)
                localView.addView(vc.localView)
                localView.setOnClickListener(View.OnClickListener { vc.toggleCaptureDevicePosition() })
                mLocalVideoViewAdded = true
                vc.setLocalVideoZOrder(!mToggleVideoViewPositions)
            }
        }
    }

    private fun addRemoteView() {
        if (mRemoteVideoViewAdded || sinchServiceInterface == null) {
            return  //early
        }
        val vc = sinchServiceInterface!!.videoController
        if (vc != null) {
            runOnUiThread {
                val remoteView = getVideoView(false)
                remoteView.addView(vc.remoteView)
                remoteView.setOnClickListener({ v: View? ->
                    removeVideoViews()
                    mToggleVideoViewPositions = !mToggleVideoViewPositions
                    addRemoteView()
                    addLocalView()
                })
                mRemoteVideoViewAdded = true
                vc.setLocalVideoZOrder(!mToggleVideoViewPositions)
            }
        }
    }

    private fun removeVideoViews() {
        if (sinchServiceInterface == null) {
            return  // early
        }
        val vc = sinchServiceInterface!!.videoController
        if (vc != null) {
            runOnUiThread {
                ((vc.remoteView.parent) as ViewGroup).removeView(vc.remoteView)
                ((vc.localView.parent) as ViewGroup).removeView(vc.localView)
                mLocalVideoViewAdded = false
                mRemoteVideoViewAdded = false
            }
        }
    }

    private fun setVideoViewsVisibility(
        localVideoVisibile: Boolean,
        remoteVideoVisible: Boolean
    ) {
        if (sinchServiceInterface == null) return
        if (!mRemoteVideoViewAdded) {
            addRemoteView()
        }
        if (!mLocalVideoViewAdded) {
            addLocalView()
        }
        val vc = sinchServiceInterface!!.videoController
        if (vc != null) {
            runOnUiThread {
                vc.localView.visibility = if (localVideoVisibile) View.VISIBLE else View.GONE
                vc.remoteView.visibility = if (remoteVideoVisible) View.VISIBLE else View.GONE
            }
        }
    }

    private inner class SinchCallListener : VideoCallListener {
        override fun onCallEnded(call: Call) {
            val cause = call.details.endCause
            Log.d(
                TAG,
                "Call ended. Reason: $cause"
            )
            mAudioPlayer!!.stopProgressTone()
            volumeControlStream = AudioManager.USE_DEFAULT_STREAM_TYPE
            val endMsg = "Call ended: " + call.details.toString()
            Toast.makeText(this@CallScreenActivity, endMsg, Toast.LENGTH_LONG).show()
            endCall()
        }

        override fun onCallEstablished(call: Call) {
            Log.d(TAG, "Call established")
            mAudioPlayer!!.stopProgressTone()
            callState!!.text = call.state.toString()
            volumeControlStream = AudioManager.STREAM_VOICE_CALL
            val audioController = sinchServiceInterface!!.audioController
            audioController?.enableSpeaker()
            if (call.details.isVideoOffered) {
                setVideoViewsVisibility(true, true)
            }
            Log.d(
                TAG,
                "Call offered video: " + call.details.isVideoOffered
            )
        }

        override fun onCallProgressing(call: Call) {
            Log.d(TAG, "Call progressing")
            mAudioPlayer!!.playProgressTone()
        }

        override fun onShouldSendPushNotification(
            call: Call,
            pushPairs: List<PushPair>
        ) {
            // Send a push through your push provider here, e.g. GCM
        }

        override fun onVideoTrackAdded(call: Call) {}
        override fun onVideoTrackPaused(call: Call) {}
        override fun onVideoTrackResumed(call: Call) {}
    }

    companion object {
        val TAG = CallScreenActivity::class.java.simpleName
        const val ADDED_LISTENER = "addedListener"
        const val VIEWS_TOGGLED = "viewsToggled"
    }
}