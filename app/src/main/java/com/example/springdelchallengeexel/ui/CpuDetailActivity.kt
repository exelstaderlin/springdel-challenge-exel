package com.example.springdelchallengeexel.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.springdelchallengeexel.R
import com.example.springdelchallengeexel.StorageUtil.getCpuInfoDetail
import kotlinx.android.synthetic.main.activity_cpu_detail.*


class CpuDetailActivity : AppCompatActivity() {

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cpu_detail)
        initObject()
        initView()
    }

    private fun initObject() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    @SuppressLint("SetTextI18n")
    private fun initView() {
        cpuInfo.text = getCpuInfoDetail()
    }
}