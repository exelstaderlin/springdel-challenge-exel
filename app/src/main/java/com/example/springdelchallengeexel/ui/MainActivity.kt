package com.example.springdelchallengeexel.ui

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.springdelchallengeexel.R
import com.example.springdelchallengeexel.StorageUtil.formatSizeToGiga
import com.example.springdelchallengeexel.StorageUtil.getFreeMemorySize
import com.example.springdelchallengeexel.StorageUtil.getFreeStorage
import com.example.springdelchallengeexel.StorageUtil.getTotalMemorySize
import com.example.springdelchallengeexel.StorageUtil.getTotalStorage
import com.example.springdelchallengeexel.model.UsageInfoData
import com.example.springdelchallengeexel.ui.rtc.LoginActivity
import com.example.springdelchallengeexel.viewmodel.MainViewModel
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.MPPointF
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.card_usage.*
import kotlinx.android.synthetic.main.loading_layout.*


open class MainActivity : AppCompatActivity() {

    private var mIncludeSystemApps = false
    private lateinit var mViewModel: MainViewModel
    private lateinit var mAdapter: MainAdapter

    override fun onCreateOptionsMenu(menu: Menu): Boolean { // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_refresh -> {
                refreshAppInfoList()
            }
            R.id.action_show_system_apps -> {
                item.isChecked = !item.isChecked
                mIncludeSystemApps = item.isChecked
                refreshAppInfoList()
            }

            R.id.cpu_info -> {
                val intent = Intent(this, CpuDetailActivity::class.java)
                startActivity(intent)
            }
            R.id.network_info -> {
                val intent = Intent(this, NetworkDetailActivity::class.java)
                startActivity(intent)
            }
            R.id.gyroscope_info -> {
                val intent = Intent(this, GyroscopeActivity::class.java)
                startActivity(intent)
            }
            R.id.location_info -> {
                val intent = Intent(this, LocationActivity::class.java)
                startActivity(intent)
            }
            R.id.vidcall_rtc -> {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initObject()
        initObserver()
        intiAdapter()
        getStorageInformation()
        getMemoryInformation()
        refreshAppInfoList()
    }

    private fun initObject() {
        mViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
    }

    private fun initObserver() {
        mViewModel.appLiveData.observe(this, Observer { appList ->
            Snackbar.make(
                recycle_view,
                appList.size.toString() + " applications loaded",
                Snackbar.LENGTH_SHORT
            ).show()
            loading_layout.visibility = View.GONE
            mAdapter.submitList(appList)
        })
    }

    private fun initChart(chart: PieChart, data: UsageInfoData) {
        chart.setUsePercentValues(true)
        chart.setDrawEntryLabels(false)
        chart.setEntryLabelColor(Color.BLACK)
        chart.setEntryLabelTextSize(12f)
        chart.setHoleColor(Color.WHITE)
        chart.setTransparentCircleColor(Color.WHITE)
        chart.setTransparentCircleAlpha(110)
        chart.setExtraOffsets(5f, 10f, 5f, 5f)
        chart.setDrawCenterText(true)
        chart.animateY(1400, Easing.EaseInOutQuad)

        chart.description.isEnabled = false
        chart.dragDecelerationFrictionCoef = 0.95f
        chart.centerText = data.info
        chart.isDrawHoleEnabled = true
        chart.holeRadius = 68f
        chart.transparentCircleRadius = 61f
        chart.isHighlightPerTapEnabled = true
        chart.isRotationEnabled = false
        setData(chart, data)
    }

    private fun setData(chart: PieChart, data: UsageInfoData) {
        val entries = ArrayList<PieEntry>()
        entries.add(PieEntry(data.totalStorage.toFloat(), "Used Space"))
        entries.add(PieEntry(data.freeStorage.toFloat(), "Free Space"))

        val dataSet = PieDataSet(entries, "")
        dataSet.setDrawIcons(false)
        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0F, 40F)
        dataSet.selectionShift = 5f

        // add a lot of colors
        val colors = ArrayList<Int>()
        val colorArr = intArrayOf(
            Color.RED,
            Color.GREEN
        )
        for (c in colorArr) colors.add(c)
        colors.add(ColorTemplate.getHoloBlue())
        dataSet.colors = colors
        val pieData = PieData(dataSet)
        pieData.setValueFormatter(PercentFormatter(chart))
        pieData.setValueTextSize(11f)
        pieData.setValueTextColor(Color.BLACK)
        chart.data = pieData
        chart.highlightValues(null)
        chart.invalidate()
    }

    private fun getStorageInformation() {
        val freeStorage = getFreeStorage(this)
        val totalStorage = getTotalStorage(this)
        val usedStorage = totalStorage - freeStorage
        val info = "${formatSizeToGiga(usedStorage)}GB / ${formatSizeToGiga(totalStorage)}GB"
        val storageData = UsageInfoData(freeStorage, totalStorage, info) //set Storage Data
        initChart(chart_storage, storageData)
    }

    private fun getMemoryInformation() {
        val freeMemory = getFreeMemorySize()
        val totalMemory = getTotalMemorySize()
        val usedMemory = totalMemory - freeMemory
        val info = "${formatSizeToGiga(usedMemory)}GB / ${formatSizeToGiga(totalMemory)}GB"
        val memoryData = UsageInfoData(freeMemory, totalMemory, info) //set Storage Data
        initChart(chart_memory, memoryData)
    }

    private fun refreshAppInfoList() {
        loading_layout.visibility = View.VISIBLE
        mViewModel.getAllInstalledApps(this, mIncludeSystemApps)
    }

    private fun intiAdapter() {
        mAdapter = MainAdapter()
        recycle_view.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycle_view.isNestedScrollingEnabled = false
        recycle_view.setHasFixedSize(true)
        recycle_view.adapter = mAdapter
    }

}
