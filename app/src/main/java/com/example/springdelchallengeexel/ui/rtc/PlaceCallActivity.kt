package com.example.springdelchallengeexel.ui.rtc

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.example.springdelchallengeexel.R
import com.example.springdelchallengeexel.service.SinchService
import kotlinx.android.synthetic.main.activity_please_call.*

class PlaceCallActivity : RtcActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_please_call)
        callButton!!.isEnabled = false
        callButton!!.setOnClickListener(buttonClickListener)
        stopButton.setOnClickListener(buttonClickListener)
    }

    override fun onServiceConnected() {
        val userName = findViewById<View>(R.id.loggedInName) as TextView
        userName.text = sinchServiceInterface!!.getUserName()
        callButton!!.isEnabled = true
    }

    private fun stopButtonClicked() {
        if (sinchServiceInterface != null) {
            sinchServiceInterface!!.stopClient()
        }
        finish()
    }

    private fun callButtonClicked() {
        val userName = callName!!.text.toString()
        if (userName.isEmpty()) {
            Toast.makeText(this, "Please enter a user to call", Toast.LENGTH_LONG).show()
            return
        }
        val call = sinchServiceInterface!!.callUserVideo(userName)
        val callId = call.callId
        val callScreen = Intent(this, CallScreenActivity::class.java)
        callScreen.putExtra(SinchService.CALL_ID, callId)
        startActivity(callScreen)
    }

    private val buttonClickListener =
        View.OnClickListener { v ->
            when (v.id) {
                R.id.callButton -> callButtonClicked()
                R.id.stopButton -> stopButtonClicked()
            }
        }
}