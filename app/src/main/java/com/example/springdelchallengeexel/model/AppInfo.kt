package com.example.springdelchallengeexel.model

import android.content.pm.ApplicationInfo
import android.graphics.drawable.Drawable

data class AppInfo(
    var label: String? = null,
    var packageName: String? = null,
    var icon: Drawable? = null
)