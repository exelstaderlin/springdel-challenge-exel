package com.example.springdelchallengeexel.model

data class UsageInfoData(
    var freeStorage: Long = 0,
    var totalStorage: Long = 0,
    var info: String = ""
)