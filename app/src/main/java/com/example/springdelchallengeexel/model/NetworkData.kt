package com.example.springdelchallengeexel.model

data class NetworkData (
    var wifiName: String = "0",
    var ipAddress: String = "0",
    var macAddress: String = "",
    var linkSpeed: String = "",
    var operatorName: String = "",
    var networkType: String = ""
)